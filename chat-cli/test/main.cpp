#include "chatbe.h"
#include "chatmq.h"

#include <iostream>
#include <glog/logging.h>

class ChatUI
{
public:
    ChatUI();
    ~ChatUI();

    void run(ChatMQ *backend_mq);
    void setMQ(ChatMQ *mq);
    ChatMQ* getMQ();
private:
     ChatMQ *mq_;
};

ChatUI::ChatUI()
{
    mq_ = new ChatMQ();
}

ChatUI::~ChatUI()
{

}

void ChatUI::setMQ(ChatMQ *mq)
{
    mq_ = mq;
}

void ChatUI::run(ChatMQ *backend_mq)
{
    LOG(INFO) << "UI thread started";
    ChatMQMessage d = {MESSAGEID_SEND_REQ, ""};
    LOG(INFO) << "Sending message MESSAGEID_SEND_REQ";
    backend_mq->push(d);

    LOG(INFO) << "Waiting for response";
    mq_->wait_and_pop(d);
    LOG(INFO) << "Response received";

    d = {MESSAGEID_EXIT_REQ, ""};
    LOG(INFO) << "Sending message MESSAGEID_EXIT_REQ";
    backend_mq->push(d);

    LOG(INFO) << "Waiting for response";
    mq_->wait_and_pop(d);
    LOG(INFO) << "Response received";

    LOG(INFO) << "exit";
}

ChatMQ* ChatUI::getMQ()
{
    return mq_;
}

int main(int argc, char *argv[])
{
    google::InitGoogleLogging(argv[0]);

    ChatBackend *backend = new ChatBackend();
    ChatUI *ui = new ChatUI();

    backend->run(ui->getMQ());
    ui->run(backend->getMQ());

    delete backend;
    delete ui;

    return 0;
}
 
