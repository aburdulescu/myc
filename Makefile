DEPS = libssl-dev libevent-dev libncurses-dev

DOCKER_IMAGE_TAG = myc
DOCKER_BUILD_OPTIONS = -t $(DOCKER_IMAGE_TAG)

ifeq ($(HTTP_PROXY),)
else
	DOCKER_BUILD_OPTIONS += --build-arg http_proxy=$(HTTP_PROXY)
endif

ifeq ($(NO_CACHE),)
else
	DOCKER_BUILD_OPTIONS += --no-cache
endif

BUILD_JUNK = \
	chat-cli/build \
	config/build \
	daemon/build \
	lib/build \

.PHONY: all install clean docker_base docker deps

all:
	cd lib; mkdir -p build; cd build; cmake ..; make
	cd config; mkdir -p build; cd build; cmake ..; make
	cd chat-cli; mkdir -p build; cd build; cmake ..; make
	cd daemon; mkdir -p build; cd build; cmake ..; make

install:
	cd lib/build; make install
	cd chat-cli/build; make install
	cd config/build; make install
	cd daemon/build; make install
	cd scripts; cp * /usr/local/bin
	ldconfig

clean:
	rm -rf $(BUILD_JUNK)

docker_base:
	[ "$(docker images -q myc-base)" = "" ] && cd docker_base && make

docker: clean Dockerfile docker_base
	docker build $(DOCKER_BUILD_OPTIONS) .

deps:
	apt update
	apt install -y $(DEPS)
