#include "myc/netaddr.h"

#include <string.h>
#include <arpa/inet.h>

#include "myc/cmn.h"


int dht_netaddr_ntop(struct sockaddr_in *n_addr, struct NetworkAddress *p_addr)
{
    int rc = 0;

    memset(p_addr->ip, 0, NETADDR_LENGTH);

    if(inet_ntop(AF_INET, &n_addr->sin_addr, p_addr->ip, NETADDR_LENGTH) == NULL)
    {
        LOG_SYSCALL_ERROR("inet_ntop");
        rc = -1;
    }
    p_addr->port = ntohs(n_addr->sin_port);

    return rc;
}

int dht_netaddr_equals(const struct NetworkAddress *addr0, const struct NetworkAddress *addr1)
{
    int rc = addr0->port == addr1->port;
    if(rc == 0)
        return 0;

    rc = strcmp(addr0->ip, addr1->ip);
    if(rc != 0)
        return 0;

    return 1;
}
