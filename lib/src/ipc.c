#include "myc/ipc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>

#include "myc/cmn.h"


int ipc_init(enum IPCEndpointType type)
{
    int s;

    s = socket(AF_UNIX, SOCK_STREAM, 0);
    if(s < 0)
    {
        LOG_SYSCALL_ERROR("socket");
        return -1;
    }

    struct sockaddr_un a;
    memset(&a, 0, sizeof(struct sockaddr_un));
    a.sun_family = AF_UNIX;
    strncpy(a.sun_path, MYC_DAEMON_SOCKET, strlen(MYC_DAEMON_SOCKET));

    int rc = 0;

    switch(type) {
    case IPC_ENDPOINT_TYPE_SERVER:
    {
        rc = unlink(MYC_DAEMON_SOCKET);
        if(rc == -1 && errno != ENOENT)
        {
            LOG_SYSCALL_ERROR("unlink");
            return -1;
        }

        printf("[.]--binding socket=%d to %s\n", s, a.sun_path);

        rc = bind(s, (struct sockaddr *)&a, sizeof(struct sockaddr_un));
        if(rc == -1)
        {
            LOG_SYSCALL_ERROR("bind");
            return -1;
        }
        listen(s, 100);
        break;
    }
    case IPC_ENDPOINT_TYPE_CLIENT:
    {
        if(connect(s, (struct sockaddr *)&a, sizeof(struct sockaddr_un)) < 0)
        {
            LOG_SYSCALL_ERROR("connect");
            return -1;
        }
        break;
    }
    default:
        break;
    }    

    return s;

}

int ipc_recv(int s, struct IPCMessage *msg)
{
    char data[IPC_MESSAGE_MAX_LENGTH];
    memset(data, 0, IPC_MESSAGE_MAX_LENGTH);

    int rc = recv(s, data, IPC_MESSAGE_MAX_LENGTH, 0);
    if(rc == -1)
    {
        LOG_SYSCALL_ERROR("recv");
        return -1;
    }
    if(rc != 0)
    {
        // unmarshall data
        memcpy(&msg->command, data, IPC_MESSAGE_COMMAND_SIZE);

        msg->data.len = rc - IPC_MESSAGE_COMMAND_SIZE;
        msg->data.value = malloc(msg->data.len);
        memcpy(msg->data.value, data + IPC_MESSAGE_COMMAND_SIZE, msg->data.len);

    }

    return rc;
}

int ipc_send(int s, const struct IPCMessage *msg)
{
    const int data_sz = IPC_MESSAGE_COMMAND_SIZE + msg->data.len;
    char data[data_sz];

    // marshall data
    memset(data, 0, IPC_MESSAGE_COMMAND_SIZE+msg->data.len);
    memcpy(data, &msg->command, IPC_MESSAGE_COMMAND_SIZE);
    memcpy(data + IPC_MESSAGE_COMMAND_SIZE, msg->data.value, msg->data.len);

    int rc = send(s, data, data_sz, 0);

    if(rc == -1)
    {
        LOG_SYSCALL_ERROR("send");
        return -1;
    }

    return rc;
}

int ipc_find(int s, const struct IPCFindRequest *req, struct IPCFindResponse *rsp)
{
    struct IPCMessage m_send;
    m_send.command = IPC_MESSAGE_COMMAND_FIND;
    m_send.data.len = sizeof(struct IPCFindRequest);
    m_send.data.value = malloc(m_send.data.len);
    memcpy(m_send.data.value, req, m_send.data.len);

    if(ipc_send(s, &m_send) < 0)
    {
        return -1;
    }
    free(m_send.data.value);

    struct IPCMessage m_recv = IPC_MESSAGE_INIT;
    if(ipc_recv(s, &m_recv) == -1)
    {
        return -1;
    }

    struct IPCFindResponse *tmp = (struct IPCFindResponse *)m_recv.data.value;
    memcpy(rsp->addr.ip, tmp->addr.ip, NETADDR_LENGTH);
    memcpy(&rsp->addr.port, &tmp->addr.port, sizeof(rsp->addr.port));

    return 0;
}
