#include "rtable.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static struct KBucket* kbucket_new();

static void contactList_copy(ContactList contacts, int *contacts_sz, struct KBucket *kb, const NodeId target);
static int contactList_compare(const void *c0, const void *c1);
static void contactList_sort(ContactList contacts, int contacts_sz);


void dht_rt_new(struct RoutingTable *rt, const NodeId id)
{
    nodeid_copy(rt->id, id);
    for(int i = 0; i < DHT_ROUTING_TABLE_SIZE; ++i)
    {
        struct KBucket *kb = kbucket_new();
        if(kb == NULL)
            return;
        rt->kbuckets[i] = kb;
    }
}

void dht_rt_findKClosest(const struct RoutingTable *rt, const NodeId id, ContactList contacts)
{
    NodeId xor;
    nodeid_xor(rt->id, id, xor);
    int kb_idx = nodeid_prefixLength(xor);
    struct KBucket *kb = rt->kbuckets[kb_idx];

    memset(contacts, 0, sizeof(ContactList));

    // add all nodes from this kbucket
    int contacts_sz = 0;
    contactList_copy(contacts, &contacts_sz, kb, id);

    if(contacts_sz < DHT_K_VALUE)
    {
        // didn't find k nodes in this kbucket => fill the remaining places from neighbouring kbuckets
        for(int i = 1;
            ((kb_idx - i) >= 0 || (kb_idx + i) < DHT_ROUTING_TABLE_SIZE)
                && contacts_sz <= DHT_K_VALUE;
            ++i)
        {
            if((kb_idx - i) >= 0)
            {
                kb = rt->kbuckets[kb_idx - i];
                contactList_copy(contacts, &contacts_sz, kb, id);
            }
            if((kb_idx + i) < DHT_ROUTING_TABLE_SIZE)
            {
                kb = rt->kbuckets[kb_idx + i];
                contactList_copy(contacts, &contacts_sz, kb, id);
            }
        }
    }

    // we have k contacts, now sort contacts by distance to target id
    contactList_sort(contacts, contacts_sz);
}

void dht_rt_print(const struct RoutingTable *rt)
{
    char nid_printable[NODE_ID_PSTRING_LENGTH];
    nodeid_btop(rt->id, nid_printable);

    printf("{\n");
    printf("\t\"nid\": \"%s\",\n", nid_printable);
    printf("\t\"kbuckets\": [\n");
    for(int i = 0; i < DHT_ROUTING_TABLE_SIZE; ++i)
    {
        if(rt->kbuckets[i]->nodes != NULL)
        {
            printf("\t\t[");
            for(struct Node* j = rt->kbuckets[i]->nodes; j; j = j->next)
            {
                char out[NODE_ID_PSTRING_LENGTH];
                nodeid_btop(j->contact.id, out);
                printf("\"%s\", ", out);
            }
            printf("]\n");
        }
    }
    printf("\t]\n");
    printf("}\n");
}

void dht_rt_delete(const struct RoutingTable *rt)
{
}

static struct KBucket* kbucket_new()
{
    struct KBucket *kb = (struct KBucket *)malloc(sizeof(struct KBucket));
    if(kb == NULL)
        return NULL;

    kb->nodes = NULL;
    kb->size = 0;

    return kb;
}

struct Node* dht_rt_kbucket_find(const struct KBucket *kb, const NodeId id)
{
    for(struct Node *i = kb->nodes; i != NULL; i = i->next)
    {
        if(nodeid_equals(i->contact.id, id))
           return i;
    }
    return NULL;
}

int dht_rt_contact_isEmpty(const struct Contact *c)
{
    int ret = 0;
    NodeId empty_id = {0};
    ret |= (nodeid_equals(c->id, empty_id)) ? 1 : 0;
//    ret |= (c->addr.ip == NULL) ? 1 : 0; // TODO: fix it
    ret |= (c->addr.port == 0) ? 1 : 0;

    return ret;
}

 struct Node* dht_rt_node_new(const NodeId id, const struct NetworkAddress *addr)
{
    struct Node *n = (struct Node *)malloc(sizeof(struct Node));
    if(n == NULL)
        return NULL;
    memset(n, 0, sizeof(struct Node));
    if(id != NULL)
        nodeid_copy(n->contact.id, id);
    if(addr != NULL)
        memcpy(&n->contact.addr, addr, sizeof(struct NetworkAddress));
    n->next = NULL;

    return n;
}

static void contactList_copy(ContactList contacts, int *contacts_sz, struct KBucket *kb, const NodeId target)
{
    for(struct Node *i = kb->nodes; i && *contacts_sz != DHT_K_VALUE; i = i->next)
    {
        memcpy(&contacts[*contacts_sz].contact, &i->contact, sizeof(struct Contact));

        NodeId distance;
        nodeid_xor(target, i->contact.id, distance);
        nodeid_copy(contacts[*contacts_sz].distance, distance);

        (*contacts_sz)++;
    }
}

static int contactList_compare(const void *c0, const void *c1)
{
    struct ContactRecord *c0_conv = (struct ContactRecord *)c0;
    struct ContactRecord *c1_conv = (struct ContactRecord *)c1;

    return nodeid_compare(c0_conv->distance, c1_conv->distance);
}

static void contactList_sort(ContactList contacts, int contacts_sz)
{
    qsort(contacts, contacts_sz, sizeof(struct ContactRecord), contactList_compare);
}

