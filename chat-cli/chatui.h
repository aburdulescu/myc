#ifndef __MYC_CHATUI__
#define __MYC_CHATUI__

#include <ncurses.h>
#include <string>

#include "chatmq.h"

enum CommandName
{
    COMMAND_NAME_UNKNOWN,
    COMMAND_NAME_CONNECT,
    COMMAND_NAME_EXIT,
};

struct Command
{
    Command():
        name(COMMAND_NAME_UNKNOWN) {}
    CommandName name;
    std::vector<std::string> args;
};

class ChatUI
{
public:
    ChatUI();
    ~ChatUI();

    void run(ChatMQ *backend_mq);

    ChatMQ* getMQ();

private:
    void init();
    void chatWin_print(std::string s);

    WINDOW *chatWin;
    WINDOW *inputWin;
    WINDOW *inputWinBox;
    WINDOW *chatWinBox;
    WINDOW *infoLine;
    WINDOW *infoLineBottom;

    void drawChatWin();
    void drawInputWin();
    void drawInfoLines();

    std::string getInput();
    Command getCommand(const std::string& input);

    static const int BOX_HEIGHT = 3;

    ChatMQ *mq_;
};

#endif
