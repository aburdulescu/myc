#ifndef __MYC_CHAT_H__
#define __MYC_CHAT_H__

#include "chatmq.h"

#include "myc/nodeid.h"
#include "myc/netaddr.h"


class ChatBackend
{
public:
    ChatBackend();
    ~ChatBackend();

    int init();
    void run(ChatMQ *ui_mq);
    ChatMQ* getMQ();

    static const int PORT = 4242;
    static const int MESSAGE_MAX_SIZE = 1024;

private:
    struct StartArgs
    {
        ChatBackend *pthis;
        ChatMQ *ui_mq;
    };

    void start(ChatMQ *ui_mq);
    static void* entry(void *args);
    int findPeer(const NodeId id, struct NetworkAddress& addr);

    int send(const std::string& message);
    int recv(std::string message);

    int socket_;

    pthread_t thread_;
    pthread_attr_t thread_attr_;
    ChatMQ *mq_;
};

#endif
