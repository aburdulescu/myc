#include "dht.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "myc/netaddr.h"

static int ping(const struct DHT *dht, const struct NetworkAddress *addr);


void dht_init(struct DHT *dht, NodeId id)
{
    dht_rt_new(&dht->rt, id);
    dht->rpc_handle = dht_rpc_init(&dht->addr);
    if(dht->rpc_handle == -1)
    {
        printf("%s: dht_rpc_init failed\n", __FUNCTION__);
    }
}

void dht_deinit(const struct DHT *dht)
{
    dht_rt_delete(&dht->rt);
    dht_rpc_deinit(dht->rpc_handle);
}

// TODO: something is wrong here
int dht_find(const struct DHT *dht, const NodeId id, struct Contact *c)
{
    NodeId xor;
    nodeid_xor(dht->rt.id, id, xor);
    int kb_idx = nodeid_prefixLength(xor);

    struct Node *kb_node = dht_rt_kbucket_find(dht->rt.kbuckets[kb_idx], id);
    if(kb_node != NULL)
    {
        memcpy(c, &kb_node->contact, sizeof(struct Contact));
        return 0;
    }
    else
    {
        // query dht
        return -1;
    }
}

int dht_bootstrap(struct DHT *dht, const struct NetworkAddress *bootstrap_addr)
{
    // for now just send FIND to bootstrap node
    struct RPCMessage m_req;
    m_req.header.type = RPC_TYPE_FIND;
    nodeid_copy(m_req.header.sender, dht->rt.id);
    m_req.data.value = malloc(MYC_NODE_ID_LENGTH);
    m_req.data.len = MYC_NODE_ID_LENGTH;
    nodeid_copy(m_req.data.value, dht->rt.id);

    int rc = dht_rpc_send(dht->rpc_handle, &m_req, bootstrap_addr);
    if(rc == -1)
        return -1;

    struct RPCMessage m_rsp;
    rc = dht_rpc_recv(dht->rpc_handle, &m_rsp, NULL);
    if(rc == -1)
        return -1;

    ContactList contacts;
    memset(contacts, 0, sizeof(contacts));
    memcpy(contacts, m_rsp.data.value, m_rsp.data.len);

    for(int i = 0; i < DHT_K_VALUE; ++i)
    {
        if(dht_rt_contact_isEmpty(&contacts[i].contact) == 0)
        {
            if(ping(dht, &contacts[i].contact.addr) == 1)
            {
                printf("[%s:%d]is alive\n", contacts[i].contact.addr.ip, contacts[i].contact.addr.port);
                dht_update(dht, &contacts[i].contact);
            }
            else
            {
                printf("[%s:%d]is not alive\n", contacts[i].contact.addr.ip, contacts[i].contact.addr.port);
            }
        }
        else
        {
            printf("contact %d is empty => ignoring\n", i);
        }
    }

    // now add the bootstrap node
    struct Contact sender;
    nodeid_copy(sender.id, m_rsp.header.sender);
    memcpy(&sender.addr, &bootstrap_addr, sizeof(bootstrap_addr));
    dht_update(dht, &sender);

    dht_rt_print(&dht->rt);
    // TODO: send find to nodes given by bootstrap node

    return 0;
}

void dht_update(struct DHT *dht, const struct Contact *c)
{
    NodeId xor;
    nodeid_xor(dht->rt.id, c->id, xor);
    int kb_idx = nodeid_prefixLength(xor);

    struct Node *kb_node = dht_rt_kbucket_find(dht->rt.kbuckets[kb_idx], c->id);
    if(kb_node != NULL)
    {
        // node is already in kbucket => move it to the front
        struct Node *tmp = dht->rt.kbuckets[kb_idx]->nodes->next;
        dht->rt.kbuckets[kb_idx]->nodes->next = kb_node->next;
        kb_node->next = tmp;
        dht->rt.kbuckets[kb_idx]->nodes = kb_node;
    }
    else
    {
        if(dht->rt.kbuckets[kb_idx]->size < DHT_K_VALUE)
        {
            // kbucket is not full => add it to end
            struct Node *i = dht->rt.kbuckets[kb_idx]->nodes;
            if(i != NULL)
            {
                for(; i->next != NULL; i = i->next);
                i->next = dht_rt_node_new(c->id, &c->addr);
            }
            else
            {
                dht->rt.kbuckets[kb_idx]->nodes = dht_rt_node_new(c->id, &c->addr);;
            }
            dht->rt.kbuckets[kb_idx]->size++;
        }
        else
        {
            printf("kbucket %d is full\n", kb_idx);
            // TODO: ping nodes starting at the end.
            //       if one node does not respond
            //          remove dead node and add the new one
            //       else
            //          discard new node

            // TODO: struct Node has to be double linked
            /* for(struct Node *i = dht->rt.kbuckets[kb_idx]->nodes; i; i = i->next) */
            /* { */
            /*     if(ping(dht, &i->contact.addr) == 1) */
            /*     { */
            /*         printf("[%s:%d]is alive\n", i->contact.addr.ip, i->contact.addr.port); */
            /*     } */
            /*     else */
            /*     { */
            /*         printf("[%s:%d]is not alive\n", i->contact.addr.ip, i->contact.addr.port); */
            /*     } */
            /* } */
        }
    }
}

static int ping(const struct DHT *dht, const struct NetworkAddress *addr)
{
    struct RPCMessage m_req;
    m_req.header.type = RPC_TYPE_PING;
    nodeid_copy(m_req.header.sender, dht->rt.id);
    m_req.data.value = NULL;
    m_req.data.len = 0;

    int rc = dht_rpc_send(dht->rpc_handle, &m_req, addr);
    if(rc == -1)
        return 0;

    struct RPCMessage m_rsp;
    struct NetworkAddress rsp_addr;
    rc = dht_rpc_recv(dht->rpc_handle, &m_rsp, &rsp_addr);
    if(rc == -1)
        return 0;

    return dht_netaddr_equals(addr, &rsp_addr);
}
