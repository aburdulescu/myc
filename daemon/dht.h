#ifndef __MYC_DHT__
#define __MYC_DHT__

/*
=============KBUCKETS========================
e = nodeid/keyid size, n = nodeid of the app, x = some nodeid
kb_i = {x if d(n,x) in [2^(e-i-1),2^(e-i))}, i in [0,e), 0<=x<n, n<x<2^e
e.g.:
e = 3
n = 5

Tree looks like this:
   /\
  /  \
 /\  /\
/\/\/\/\
01234567

Now, calculate the distances between n and all other nodes:

|---+-----------+---------+---------------------|
| i | 2^(e-i-1) | 2^(e-i) | [2^(e-i-1),2^(e-i)) |
|---+-----------+---------+---------------------|
| 0 | 2^2=4     | 2^3=8   | [4,8)               |
| 1 | 2^1=2     | 2^2=4   | [2,4)               |
| 2 | 2^0=1     | 2^1=2   | [1,2)               |
|---+-----------+---------+---------------------|


|-------+--------+
| x     | d(n,x) |
|-------+--------+
| 5=101 | n.a.   |
|-------+--------+
| 0=000 | 101=5  |
| 1=001 | 100=4  |
| 2=010 | 111=7  |
| 3=011 | 110=6  |
| 4=100 | 001=1  |
| 6=110 | 011=3  |
| 7=111 | 010=2  |
|-------+--------+

So, using the tables defined above we get:

kb_0 = {x for x in {0,1,2,3]}
kb_1 = {x for x in {6,7}}
kb_2 = {x for x in {4}}

Note:
Given a nodeid x, we can determine the kbucket in which this node x should be
placed by calculating the number of leading zeros from the nodeid resulted by
calculating the distance between x and our nodeid.
e.g.:
n = 5  = 101
x = 2  = 010
r = d(n,x) = 111
leading_zeros(r) = 0 => x should go to kbucket 0
*/

/*
=============NODE LOOKUP=================
lookup(RoutingTable r, NodeId x):
  xor = d(r.n,x)
  i = leading_zeros(xor)
  if kbucket_find(kb[i], x) == true:
    return found_node
  else
    nodes = kbucket_get_nodes(kb[i], alpha) // alpha from Kademlia paper
    for n in nodes:
      new_nodes = kademlia_find_node(n, x)
      if node_find(nodes, x) == true:
        return found_node
      else
        nodes.append(new_nodes)

The above pseudocode is just a draft(concurency has to be taken into consideration etc.).
 */

#include "rtable.h"
#include "rpc.h"

struct DHT
{
    struct RoutingTable rt;
    struct NetworkAddress addr;
    dht_rpc_t rpc_handle;
};

void dht_init(struct DHT *dht, NodeId id);
void dht_deinit(const struct DHT *dht);
int  dht_bootstrap(struct DHT *dht, const struct NetworkAddress *bootstrap_addr);
int  dht_find(const struct DHT *dht, const NodeId id, struct Contact *c);
void dht_update(struct DHT *dht, const struct Contact *c);

#endif
