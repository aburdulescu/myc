#ifndef __MYC_DHT_RTABLE__
#define __MYC_DHT_RTABLE__

#include "myc/nodeid.h"
#include "myc/netaddr.h"


#define DHT_K_VALUE 20
#define DHT_ALPHA_VALUE 3
#define DHT_ROUTING_TABLE_SIZE (MYC_NODE_ID_LENGTH * 8)


struct Contact
{
    NodeId id;
    struct NetworkAddress addr;
};

struct Node
{
    struct Contact contact;
    struct Node *next;
};

struct KBucket
{
    struct Node *nodes;
    int size;
};

struct RoutingTable
{
    struct KBucket* kbuckets[DHT_ROUTING_TABLE_SIZE];
    NodeId id;
};

struct ContactRecord
{
    struct Contact contact;
    NodeId distance;
};

typedef struct ContactRecord ContactList[DHT_K_VALUE];

void dht_rt_new(struct RoutingTable *rt, const NodeId id);
void dht_rt_findKClosest(const struct RoutingTable *rt, const NodeId id, ContactList contacts);
void dht_rt_print(const struct RoutingTable *rt);
void dht_rt_delete(const struct RoutingTable *rt);

struct Node* dht_rt_kbucket_find(const struct KBucket *kb, const NodeId id);

int dht_rt_contact_isEmpty(const struct Contact *c);

struct Node* dht_rt_node_new(const NodeId id, const struct NetworkAddress *addr);

#endif
