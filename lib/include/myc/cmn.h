#ifndef __MYC_CMN__
#define __MYC_CMN__

#include <stdio.h>
#include <errno.h>
#include <string.h>


#ifdef __cplusplus
extern "C"{
#endif

#define LOG_SYSCALL_ERROR(syscall) \
    printf("[.]--syscall error in %s():%s(): errno = %d, %s\n",\
           __FUNCTION__, syscall, errno, strerror(errno))

    
#ifdef __cplusplus
}
#endif

#endif
