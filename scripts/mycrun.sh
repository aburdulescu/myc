#!/bin/bash

if [[ $# -eq 0 ]]
then
    echo "without parameter => run as bootstrap node"
    myccfg init
    echo "init done => start daemon"
    mycd
else
    echo "with parameter $1 => run as normal node"
    BOOTSTRAP_IP=172.17.0.2

    myccfg init
    echo "init done => start daemon"
    mycd $BOOTSTRAP_IP $1 1>/tmp/mycd.log 2>&1 &
fi
