#include "chatui.h"

#include <iostream>
#include <ctype.h>
#include <glog/logging.h>

ChatUI::ChatUI()
{
    mq_ = new ChatMQ();
}

ChatUI::~ChatUI()
{
    endwin();
}

void ChatUI::run(ChatMQ *backend_mq)
{
    LOG(INFO) << "UI thread started";

    init();

    bool exitNeeded = false;

    while(!exitNeeded)
    {
        wcursyncup(stdscr);
        wrefresh(chatWin);
        wrefresh(inputWin);

        std::string input = getInput();

        if(input[0] == '/') // it's a command
        {
            Command cmd = getCommand(input);

            switch (cmd.name) {
            case COMMAND_NAME_UNKNOWN:
            {
                chatWin_print("error: Unknown command " + input);
                break;
            }
            case COMMAND_NAME_CONNECT:
            {
                ChatMQMessage d = {MESSAGEID_CONNECT_REQ, cmd.args[0]};
                backend_mq->push(d);
                chatWin_print("Connecting to " + cmd.args[0]);
                mq_->wait_and_pop(d);
                // check received message?
                break;
            }
            case COMMAND_NAME_EXIT:
            {
                ChatMQMessage d = {MESSAGEID_EXIT_REQ, ""};
                backend_mq->push(d);
                mq_->wait_and_pop(d);
                // check received message?
                exitNeeded = true;
                break;
            }
            }
        }
        else // it's a message => print it in chatWin
        {
            ChatMQMessage d = {MESSAGEID_SEND_REQ, input};
            backend_mq->push(d);
            mq_->wait_and_pop(d);
            // check received message?
            chatWin_print(input);
        }
    }

    LOG(INFO) << "exiting";
}

ChatMQ* ChatUI::getMQ()
{
    return mq_;
}

void ChatUI::init()
{
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);

    drawChatWin();
    drawInputWin();
    drawInfoLines();
    refresh();
}

void ChatUI::drawChatWin()
{
    chatWinBox = subwin(stdscr, (LINES * 0.8), COLS, 0, 0);
    box(chatWinBox, 0, 0);
    wrefresh(chatWinBox);
    chatWin = subwin(chatWinBox, (LINES * 0.8 - 2), COLS - 2, 1, 1);
    scrollok(chatWin, TRUE);
    touchwin(stdscr);
    wrefresh(chatWin);
}

void ChatUI::drawInputWin()
{
    inputWinBox = subwin(stdscr, (LINES * 0.2) - 1, COLS, (LINES * 0.8) + 1, 0);
    box(inputWinBox, 0, 0);
    inputWin = subwin(inputWinBox, (LINES * 0.2) - 3, COLS - 2, (LINES * 0.8) + 2, 1);
    wrefresh(inputWin);
}

void ChatUI::drawInfoLines()
{
    infoLine = subwin(stdscr, 1, COLS, (LINES * 0.8), 0);
    wprintw(infoLine, " Type /help to view a list of available commands");
    wrefresh(infoLine);
    infoLineBottom = subwin(stdscr, 1, COLS, LINES - 1, 0);
    wrefresh(infoLineBottom);
}

void ChatUI::chatWin_print(std::string s)
{
    s.push_back('\n');
    wprintw(chatWin, s.c_str());
}

std::string ChatUI::getInput()
{
    std::string input = "";

    wmove(inputWin, 0, 0);
    wrefresh(inputWin);

    int ch;
    while ((ch = getch()) != '\n')
    {
        // Backspace
        if (ch == 8 || ch == 127 || ch == KEY_LEFT)
        {
            if (input.size() > 0)
            {
                input.pop_back();
                wprintw(inputWin, "\b \b\0");
                wrefresh(inputWin);
            }
            // else
            // {
            //     wprintw(inputWin, "\b \0");
            // }
        }
        else if (ch != ERR)
        {
            input.push_back(ch);
            wprintw(inputWin, (char *)&ch);
            wrefresh(inputWin);
        }
    }

    wclear(inputWin);
    wrefresh(inputWin);

    return input;
}

Command ChatUI::getCommand(const std::string& input)
{
    Command cmd;

    size_t i = input.find(' ', 1);
    std::string cmdStr;

    if(i == std::string::npos) // no args
    {
        cmdStr = input.substr(1, i);
    }
    else
    {
        cmdStr = input.substr(1, i-1);

        int start = i+1;
        while(start != -1)
        {
            i = input.find(' ', start);
            if(i == std::string::npos)
            {
                if(input[i-1] != ' ')
                    cmd.args.push_back(input.substr(start, i));
                start = -1;
            }
            else
            {
                if(input[i-1] != ' ')
                    cmd.args.push_back(input.substr(start, i-start));
                start = i+1;
            }
        }
    }

    if(cmdStr == "connect" || cmdStr == "c")
        cmd.name = COMMAND_NAME_CONNECT;
    else if(cmdStr == "exit" || cmdStr == "e")
        cmd.name = COMMAND_NAME_EXIT;

    LOG(INFO) << "input=" << input << " "
              << "cmdStr=" << cmdStr;
    LOG(INFO) << "cmd.args:";
    for(auto arg : cmd.args)
        LOG(INFO) << arg;

    return cmd;
}
