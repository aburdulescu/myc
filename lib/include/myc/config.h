#ifndef __MYC_CONFIG__
#define __MYC_CONFIG__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#ifdef __cplusplus
extern "C"{
#endif

#define MYC_KEY_SIZE 2048

#define MYC_CONFIG_DIRNAME       ".myc/"
#define MYC_PRIVATE_KEY_FILENAME "private.pem"
#define MYC_PUBLIC_KEY_FILENAME  "public.pem"
#define MYC_NODE_ID_FILENAME     "node_id"

#define CONFIG_VARIABLE_LENGTH 255

struct config_variables
{
    char config_dirpath[CONFIG_VARIABLE_LENGTH];
    char private_key_filepath[CONFIG_VARIABLE_LENGTH];
    char public_key_filepath[CONFIG_VARIABLE_LENGTH];
    char node_id_filepath[CONFIG_VARIABLE_LENGTH];
};

void get_config_variables(struct config_variables *cfg_vars)
{
    char *home = getenv("HOME");
    size_t home_sz = strlen(home);

    size_t MYC_CONFIG_DIRNAME_sz = strlen(MYC_CONFIG_DIRNAME);
    size_t config_dirpath_sz = home_sz + MYC_CONFIG_DIRNAME_sz + 1;

    memset(cfg_vars->config_dirpath, 0, CONFIG_VARIABLE_LENGTH);
    memcpy(cfg_vars->config_dirpath, home, home_sz);
    strncat(cfg_vars->config_dirpath, "/", 1);
    strncat(cfg_vars->config_dirpath, MYC_CONFIG_DIRNAME, MYC_CONFIG_DIRNAME_sz);

    memset(cfg_vars->private_key_filepath, 0, CONFIG_VARIABLE_LENGTH);
    memcpy(cfg_vars->private_key_filepath, cfg_vars->config_dirpath, config_dirpath_sz);
    strncat(cfg_vars->private_key_filepath, MYC_PRIVATE_KEY_FILENAME, strlen(MYC_PRIVATE_KEY_FILENAME));

    memset(cfg_vars->public_key_filepath, 0, CONFIG_VARIABLE_LENGTH);
    memcpy(cfg_vars->public_key_filepath, cfg_vars->config_dirpath, config_dirpath_sz);
    strncat(cfg_vars->public_key_filepath, MYC_PUBLIC_KEY_FILENAME, strlen(MYC_PUBLIC_KEY_FILENAME));

    memset(cfg_vars->node_id_filepath, 0, CONFIG_VARIABLE_LENGTH);
    memcpy(cfg_vars->node_id_filepath, cfg_vars->config_dirpath, config_dirpath_sz);
    strncat(cfg_vars->node_id_filepath, MYC_NODE_ID_FILENAME, strlen(MYC_NODE_ID_FILENAME));
}

    
#ifdef __cplusplus
}
#endif

#endif
