#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <event2/event.h>
#include <pthread.h>
#include <signal.h>

#include "myc/cmn.h"
#include "myc/config.h"
#include "myc/ipc.h"
#include "myc/nodeid.h"

#include "dht.h"


#define MAX_CONN 10

struct DHTThreadArgs
{
    struct DHT *dht;
    int ipc_handle;
    NodeId id;
    pthread_mutex_t *mutex;
};


int   read_nodeid(NodeId id);
void  on_dht_event(evutil_socket_t fd, short what, void *args);
void  on_ipc_request_event(evutil_socket_t fd, short what, void *eb);
void  on_ipc_connection_event(evutil_socket_t fd, short what, void *args);
void  handle_ipc_find_command(const NodeId nid, evutil_socket_t ipc);
void* run_dht_find(void* data);
void  sigint_handler(int signum, siginfo_t *info, void *ptr);
void  catch_sigint();

struct config_variables cfg_vars;
struct DHT dht;
pthread_mutex_t dht_mutex;
int dht_thread_count;

int main(int argc, char *argv[])
{
    catch_sigint();

    int bootstrap_needed = 0;
    struct NetworkAddress bootstrap_addr;
    memset(&bootstrap_addr, 0, sizeof(struct NetworkAddress));
    if(argc == 3)
    {
        bootstrap_needed = 1;
        memcpy(bootstrap_addr.ip, argv[1], strlen(argv[1]));
        bootstrap_addr.port = atoi(argv[2]);
    }

    get_config_variables(&cfg_vars);

    NodeId id;
    if(read_nodeid(id) == -1)
        return 1;

    dht_init(&dht, id);

    int ipc_handle = ipc_init(IPC_ENDPOINT_TYPE_SERVER);
    if(ipc_handle == -1)
        return 1;

    evutil_make_socket_nonblocking(dht.rpc_handle);
    evutil_make_socket_nonblocking(ipc_handle);

    struct event_base *eb = event_base_new();
    if(eb == NULL)
    {
        printf("Failed to create event_base\n");
        return 1;
    }

    event_add(event_new(eb, dht.rpc_handle, EV_READ | EV_PERSIST, on_dht_event, eb), NULL);
    event_add(event_new(eb, ipc_handle, EV_READ | EV_PERSIST, on_ipc_connection_event, eb), NULL);

    if(bootstrap_needed)
    {
        printf("bootstraping with node at %s:%d\n", bootstrap_addr.ip, bootstrap_addr.port);

        pthread_mutex_lock(&dht_mutex);
        int rc = dht_bootstrap(&dht, &bootstrap_addr);
        pthread_mutex_unlock(&dht_mutex);

        if(rc == -1)
            return 1;
    }
    else
    {
        printf("Running as bootstrap node on [%s:%d]\n", dht.addr.ip, dht.addr.port);
    }

    int rc = event_base_dispatch(eb);

    return (rc == 0) ? 0 : 1;
}

int read_nodeid(NodeId id)
{
    FILE *fp = fopen(cfg_vars.node_id_filepath, "rb");
    if(fp == NULL)
    {
        printf("error: %s\n", strerror(errno));
        return -1;
    }

    fread(id, sizeof(unsigned char), MYC_NODE_ID_LENGTH, fp);

    fclose(fp);

    return 0;
}

void on_dht_event(evutil_socket_t fd, short what, void *args)
{
    struct RPCMessage m_recv;
    struct NetworkAddress addr;
    if(dht_rpc_recv(fd, &m_recv, &addr) == -1)
        return;

    struct RPCMessage m_send;

    NodeId wanted_id;
    char nid_printable[2*MYC_NODE_ID_LENGTH];

    int rc = 0;

    switch(m_recv.header.type)
    {
    case RPC_TYPE_PING:
        printf("[]<-[%s:%d]PING\n", addr.ip, addr.port);
        m_send.header.type = RPC_TYPE_PING;
        nodeid_copy(m_send.header.sender, dht.rt.id);
        m_send.data.value = NULL;
        m_send.data.len = 0;

        rc = dht_rpc_send(dht.rpc_handle, &m_send, &addr);
        break;
    case RPC_TYPE_FIND:
        nodeid_copy(wanted_id, m_recv.data.value);
        nodeid_btop(wanted_id, nid_printable);

        printf("[]<-[%s:%d]FIND %s\n", addr.ip, addr.port, nid_printable);

        ContactList contacts;
        dht_rt_findKClosest(&dht.rt, wanted_id, contacts);

        m_send.header.type = RPC_TYPE_FIND;
        nodeid_copy(m_send.header.sender, dht.rt.id);
        m_send.data.len = sizeof(ContactList);
        m_send.data.value = malloc(m_send.data.len);
        memcpy(m_send.data.value, contacts, m_send.data.len);

        rc = dht_rpc_send(dht.rpc_handle, &m_send, &addr);

        struct Contact sender;
        nodeid_copy(sender.id, m_recv.header.sender);
        memcpy(&sender.addr, &addr, sizeof(addr));
        dht_update(&dht, &sender);

        break;
    default:
        break;
    }
}

void on_ipc_request_event(evutil_socket_t fd, short what, void *eb)
{
    struct IPCMessage m_recv = IPC_MESSAGE_INIT;

    int rc = ipc_recv(fd, &m_recv);

    switch(rc) {
    case 0:
    {
        printf("[.]--[%d]host disconnected\n", fd);
        close(fd);
        break;
    }
    case -1:
    {
        close(fd);
        break;
    }
    default:
    {
        switch(m_recv.command) {
        case IPC_MESSAGE_COMMAND_FIND:
        {
            struct IPCFindRequest req;
            memcpy(&req, m_recv.data.value, m_recv.data.len);

            // each request gets it's own thread
            // how many request can handle?
            // TODO: queue requests
            handle_ipc_find_command(req.id, fd);
            break;
        }
        default:
            close(fd);
            break;
        }
        break;
    }
    }
}

void on_ipc_connection_event(evutil_socket_t fd, short what, void *args)
{
    int new_fd = accept(fd, NULL, NULL);
    if (new_fd < 0)
    {
        LOG_SYSCALL_ERROR("accept");
        return;
    }

    printf("[.]--new connection, socket=%d\n" , new_fd);

    struct event_base *eb = (struct event_base *)args;
    event_add(event_new(eb, new_fd, EV_READ, on_ipc_request_event, NULL), NULL);
}

void  handle_ipc_find_command(const NodeId nid, evutil_socket_t ipc)
{
    // spawn a separate thread to handle the request
    struct DHTThreadArgs *args = (struct DHTThreadArgs *)malloc(sizeof(struct DHTThreadArgs));

    args->dht = &dht;
    args->mutex = &dht_mutex;
    args->ipc_handle = ipc;
    nodeid_copy(args->id, nid);

    pthread_t t;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    pthread_create(&t, &attr, run_dht_find, args);
}

void* run_dht_find(void* data)
{
    struct DHTThreadArgs * args= (struct DHTThreadArgs *)data;

    pthread_mutex_lock(args->mutex);
    dht_thread_count++;
    int thread_no = dht_thread_count;
    pthread_mutex_unlock(args->mutex);

    char nid_printable[NODE_ID_PSTRING_LENGTH];
    nodeid_btop(args->id, nid_printable);

    printf("[.][%d]<-[%d]Search for: %s\n", thread_no, args->ipc_handle, nid_printable);

    struct Contact c;

    int rc = 0;
    pthread_mutex_lock(args->mutex);
    rc = dht_find(args->dht, args->id, &c);
    pthread_mutex_unlock(args->mutex);

    struct IPCFindResponse rsp;
    memset(&rsp, 0, sizeof(struct IPCFindResponse));
    if(rc == 0)
    {
        memcpy(rsp.addr.ip, c.addr.ip, NETADDR_LENGTH);
        rsp.addr.port = c.addr.port;
    }
    else
    {
        printf("[.][%d]--peer not found\n", thread_no);
    }

    struct IPCMessage m_send = IPC_MESSAGE_INIT;
    m_send.command = IPC_MESSAGE_COMMAND_FIND;
    m_send.data.len = sizeof(struct IPCFindResponse);
    m_send.data.value = malloc(m_send.data.len);
    memcpy(m_send.data.value, &rsp, m_send.data.len);

    rc = ipc_send(args->ipc_handle, &m_send);
    if(rc != -1)
    {
        printf("[.][%d]->[%d]%s:%d\n", thread_no, args->ipc_handle, rsp.addr.ip, rsp.addr.port);
    }

    free(m_send.data.value);

    free(args);
    close(args->ipc_handle);

    pthread_mutex_lock(args->mutex);
    dht_thread_count--;
    pthread_mutex_unlock(args->mutex);

    pthread_exit(NULL);
}

void sigint_handler(int signum, siginfo_t *info, void *ptr)
{
    printf("\nSIGINT caught, bye!\n");
    exit(0);
}

void catch_sigint()
{
    static struct sigaction _sigact;

    memset(&_sigact, 0, sizeof(_sigact));
    _sigact.sa_sigaction = sigint_handler;
    _sigact.sa_flags = SA_SIGINFO;

    sigaction(SIGINT, &_sigact, NULL);
}
