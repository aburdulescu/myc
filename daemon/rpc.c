#include "rpc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>

#include "myc/cmn.h"


dht_rpc_t dht_rpc_init(struct NetworkAddress *addr)
{
    dht_rpc_t handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (handle == -1)
    {
        LOG_SYSCALL_ERROR("socket");
        return -1;
    }

    int rc = 0;

    struct sockaddr_in sin;
    memset((char *) &sin, 0, sizeof(sin));
     
    sin.sin_family = AF_INET;
    sin.sin_port = 0;
    sin.sin_addr.s_addr = htonl(INADDR_ANY);

    rc = bind(handle, (struct sockaddr*)&sin, sizeof(sin));
    if(rc == -1)
    {
        LOG_SYSCALL_ERROR("bind");
        close(handle);
    }

    struct sockaddr_in raw_addr;
    socklen_t raw_addr_len = sizeof(struct sockaddr_in);
    rc = getsockname(handle, (struct sockaddr *)&raw_addr, &raw_addr_len);
    if(rc == -1)
    {
        LOG_SYSCALL_ERROR("getsockname");
        close(handle);
    }

    rc = dht_netaddr_ntop(&raw_addr, addr);

    return (rc == -1) ? rc : handle;
}

int dht_rpc_recv(dht_rpc_t handle, struct RPCMessage *m, struct NetworkAddress *addr)
{
    char data[RPC_MESSAGE_MAX_LENGTH];
    memset(data, 0, RPC_MESSAGE_MAX_LENGTH);

    struct sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));
    socklen_t sin_len = sizeof(sin);

    int rc = recvfrom(handle, data, RPC_MESSAGE_MAX_LENGTH, 0, (struct sockaddr *)&sin, &sin_len);
    if(rc == -1)
    {
        LOG_SYSCALL_ERROR("recvfrom");
    }

    if(rc != 0)
    {
        memcpy(&m->header.type, data, RPC_TYPE_SIZE);
        memcpy(&m->header.sender, data + RPC_TYPE_SIZE, MYC_NODE_ID_LENGTH);

        m->data.len = rc - RPC_HEADER_SIZE;
        if(m->data.len != 0)
        {
            m->data.value = malloc(m->data.len);
            memcpy(m->data.value, data + RPC_HEADER_SIZE, m->data.len);
        }
    }
    if(addr != NULL)
        rc = dht_netaddr_ntop(&sin, addr);

    return rc;
}

int dht_rpc_send(dht_rpc_t handle, const struct RPCMessage *m, const struct NetworkAddress *addr)
{
    struct sockaddr_in sin;
    socklen_t sin_len = sizeof(sin);
    memset(&sin, 0, sizeof(struct sockaddr_in));
    sin.sin_port = htons(addr->port);

    int rc = inet_pton(AF_INET, addr->ip, &sin.sin_addr);
    if(rc == 0)
    {
        printf("%s: bad IP address format\n", __FUNCTION__);
        return -1;
    }
    else if(rc == -1)
    {
        LOG_SYSCALL_ERROR("inet_pton");
        return -1;
    }

    const int data_sz = RPC_HEADER_SIZE + m->data.len;
    char data[data_sz];

    // marshall data
    memset(data, 0, RPC_HEADER_SIZE + m->data.len);
    memcpy(data, &m->header.type, RPC_TYPE_SIZE);
    memcpy(data + RPC_TYPE_SIZE, m->header.sender, MYC_NODE_ID_LENGTH);
    if(m->data.len != 0)
        memcpy(data + RPC_HEADER_SIZE, m->data.value, m->data.len);

    rc = sendto(handle, data, data_sz, 0, (struct sockaddr*)&sin, sin_len);
    if(rc == -1)
    {
        LOG_SYSCALL_ERROR("sendto");
    }

    return rc;
}

void dht_rpc_deinit(dht_rpc_t handle)
{
    close(handle);
}
