#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "nodeid.h"

typedef unsigned char NodeId[4];

int nodeid_getBucketNumber(const NodeId nid)
{
    for(int i = 0; i < 4; ++i)
        for(unsigned char j = 0; j < 8; j++)
        {
            unsigned char r = nid[i]>>(unsigned char)(7 - j) & 0x01;
            if(r != 0)
                return i * 8 +j;
        }
    return 4 * 8 - 1;
}
void nodeid_btop(const NodeId nid, char out[9])
{
    memset(out, 0, 9);
    char tmp[3];
    for(int i = 0; i < 4; ++i)
    {
        memset(tmp, 0, 3);
        sprintf(tmp, "%02x", nid[i]);
        out[2*i] = tmp[0];
        out[2*i+1] = tmp[1];
    }
}

void test_getBucketNumber(NodeId nid)
{
    char out[9];
    nodeid_btop(nid, out);
    printf("nid = %s, bucketNumber = %d\n", out, nodeid_getBucketNumber(nid));
}

int main(int argc, char *argv[argc])
{
    NodeId nid0 = {0, 0, 0, 0};
    test_getBucketNumber(nid0);

    NodeId nid1 = {0, 0, 0, 1};
    test_getBucketNumber(nid1);

    NodeId nid2 = {1, 0, 0, 0};
    test_getBucketNumber(nid2);

    NodeId nid3 = {0xff, 0, 0, 0};
    test_getBucketNumber(nid3);

    return 0;
}
