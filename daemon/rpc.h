#ifndef __MYC_DHT_RPC__
#define __MYC_DHT_RPC__

#include "rtable.h"
#include "myc/netaddr.h"
#include "myc/nodeid.h"

// TODO: maybe use protobuf?
enum RPCType
{
    RPC_TYPE_PING,
    RPC_TYPE_FIND,
};

struct RPCHeader
{
    enum RPCType type;
    NodeId sender;
};

struct RPCMessageData
{
    void *value;
    int len;
};

struct RPCMessage
{
    struct RPCHeader header;
    struct RPCMessageData data;
};

#define RPC_TYPE_SIZE (sizeof(enum RPCType))
#define RPC_HEADER_SIZE (sizeof(enum RPCType) + MYC_NODE_ID_LENGTH)
#define RPC_MESSAGE_MAX_LENGTH (RPC_HEADER_SIZE + sizeof(ContactList))

typedef int dht_rpc_t;

dht_rpc_t dht_rpc_init(struct NetworkAddress *addr);
void      dht_rpc_deinit(dht_rpc_t handle);
int       dht_rpc_recv(dht_rpc_t handle, struct RPCMessage *m, struct NetworkAddress *addr);
int       dht_rpc_send(dht_rpc_t handle, const struct RPCMessage *m, const struct NetworkAddress *addr);

#endif
