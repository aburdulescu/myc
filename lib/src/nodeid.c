#include "myc/nodeid.h"

#include <string.h>
#include <openssl/evp.h>


static int generate_hash(const char           *digest_name,
                         const unsigned char  *data,
                         const int             data_sz,
                         unsigned char       **hash,
                         int                  *hash_len);
static int convert_pchar(char pchar);

int nodeid_new(NodeId nid, const unsigned char *data, const int data_sz)
{
    int            rc          = 0;
    unsigned char *hash        = NULL;
    int            hash_len    = 0;

    if(generate_hash(MYC_HASH_ALGORITHM, data, data_sz, &hash, &hash_len) == -1)
    {
        printf("error: hash_generate()\n");
        rc = -1;
        goto cleanup;
    }
    if(hash_len != MYC_NODE_ID_LENGTH)
    {
        printf("error: %s: hash_len != MYC_NODE_ID_LENGTH\n", __FUNCTION__);
        rc = -1;
        goto cleanup;
    }
    memcpy(nid, hash, hash_len);

cleanup:
    free(hash);

    return rc;
}

void nodeid_btop(const NodeId nid, char out[NODE_ID_PSTRING_LENGTH])
{
    memset(out, 0, NODE_ID_PSTRING_LENGTH);
    char tmp[3];
    for(int i = 0; i < MYC_NODE_ID_LENGTH; ++i)
    {
        memset(tmp, 0, 3);
        sprintf(tmp, "%02x", nid[i]);
        out[2*i] = tmp[0];
        out[2*i+1] = tmp[1];
    }
}

int nodeid_ptob(const char *pnid, const int pnid_len, NodeId nid)
{
    if(pnid_len != (2 * MYC_NODE_ID_LENGTH))
        return -1;
    for(int i = 0; i < MYC_NODE_ID_LENGTH; ++i)
    {
        nid[i] = (convert_pchar(pnid[2*i]) << 4) | convert_pchar(pnid[2*i+1]);
    }
    return 0;
}

static int generate_hash(const char *digest_name,
                         const unsigned char *data,
                         const int data_sz,
                         unsigned char **hash,
                         int *hash_len)
{
    if(digest_name == NULL)
    {
        printf("error: %s: digest_name == NULL\n", __FUNCTION__);
        return -1;
    }
    if(data == NULL)
    {
        printf("error: %s: data == NULL\n", __FUNCTION__);
        return -1;
    }

    int           rc    = 0;
    const EVP_MD *md    = NULL;
    EVP_MD_CTX   *mdctx = NULL;
    
    md = EVP_get_digestbyname(digest_name);
    if(md == NULL)
    {
        printf("error: unknown message digest %s\n", digest_name);
        rc = -1;
        goto cleanup;
    }

    mdctx = EVP_MD_CTX_new();

    EVP_DigestInit_ex(mdctx, md, NULL);
    EVP_DigestUpdate(mdctx, data, data_sz);

    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;
    EVP_DigestFinal_ex(mdctx, md_value, &md_len);

    *hash = (unsigned char *)malloc(md_len);
    memcpy(*hash, md_value, md_len);

    *hash_len = md_len;

cleanup:
    EVP_MD_CTX_free(mdctx);

    return rc;
}

// convert hex char to binary char
static int convert_pchar(char pchar)
{
    unsigned char ret = 0;
    switch(pchar)
    {
    case '0': ret = 0; break;
    case '1': ret = 1; break;
    case '2': ret = 2; break;
    case '3': ret = 3; break;
    case '4': ret = 4; break;
    case '5': ret = 5; break;
    case '6': ret = 6; break;
    case '7': ret = 7; break;
    case '8': ret = 8; break;
    case '9': ret = 9; break;
    case 'a': ret = 10; break;
    case 'b': ret = 11; break;
    case 'c': ret = 12; break;
    case 'd': ret = 13; break;
    case 'e': ret = 14; break;
    case 'f': ret = 15; break;
    default:
        break;
    };
    return ret;
}

int nodeid_prefixLength(const NodeId nid)
{
    for(int i = 0; i < MYC_NODE_ID_LENGTH; ++i)
        for(unsigned char j = 0; j < 8; j++)
            if((nid[i]>>(unsigned char)(7 - j) & 0x01) == 0x01)
                return i * 8 + j;
    return MYC_NODE_ID_LENGTH * 8 - 1;
}

void nodeid_xor(const NodeId nid0, const NodeId nid1, NodeId res_nid)
{
    for(int i = 0; i < MYC_NODE_ID_LENGTH; ++i)
    {
        res_nid[i] = nid0[i] ^ nid1[i];
    }
}

int nodeid_equals(const NodeId nid0, const NodeId nid1)
{
    for(int i = 0; i < MYC_NODE_ID_LENGTH; ++i)
        if(nid0[i] != nid1[i])
            return 0;
    return 1;
}

int nodeid_compare(const NodeId nid0, const NodeId nid1)
{
    int i = 0;

    for(; i < MYC_NODE_ID_LENGTH && nid0[i] == nid1[i]; ++i);

    if(i == MYC_NODE_ID_LENGTH)
        return 0;
    else if(nid0[i] > nid1[i])
        return -1;
    else
        return 1;
}

void nodeid_copy(NodeId dest, const NodeId src)
{
    memcpy(dest, src, MYC_NODE_ID_LENGTH);
}

