#ifndef __MYC_IPC__
#define __MYC_IPC__

#include "myc/nodeid.h"
#include "myc/netaddr.h"


#ifdef __cplusplus
extern "C"{
#endif

#define MYC_DAEMON_SOCKET "/tmp/mycd"

enum IPCMessageCommand
{
    IPC_MESSAGE_COMMAND_UNKNOWN,
    IPC_MESSAGE_COMMAND_FIND,
};

#define IPC_MESSAGE_COMMAND_SIZE sizeof(enum IPCMessageCommand)

struct IPCFindRequest
{
    NodeId id;
};

struct IPCFindResponse
{
    struct NetworkAddress addr;
};

struct IPCMessageData
{
    void *value;
    int len;
};

struct IPCMessage
{
    enum IPCMessageCommand command;
    struct IPCMessageData data;
};

enum IPCEndpointType
{
    IPC_ENDPOINT_TYPE_UNKNOWN,
    IPC_ENDPOINT_TYPE_SERVER,
    IPC_ENDPOINT_TYPE_CLIENT,
};

#define MAX(x, y) ((x > y) ? (x) : (y))

#define IPC_MESSAGE_INIT {IPC_MESSAGE_COMMAND_UNKNOWN, {NULL, 0}}
#define IPC_MESSAGE_MAX_LENGTH (IPC_MESSAGE_COMMAND_SIZE + MAX(sizeof(NodeId), sizeof(struct NetworkAddress)))

/*
Example for FIND command:

//=======SENDER SIDE===========
// we have the peer nodeid and the message that we want to pass to the peer
NodeId peerid = someid; // 34f2da3.....
char msg[] = "hello friend!";

//now we can call ipc_send(ipc_msg)

//=======RECEIVER SIDE===========

// declare a ipc_msg
struct IPCMessage ipc_msg = IPC_MESSAGE_INIT;

// receive ipc_msg
ipc_recv(ipc_msg);

// check the type of command that the ipc_msg contains
switch(ipc_msg.command) {
case IPC_MESSAGE_COMMAND_FIND:
    struct IPCRequestFind *cmd = (struct IPCRequestFind *)ipc_msg.data;

    // print the message
    printf("msg = %s\n", cmd->message);
    break;
default:
    break;
}

*/


int ipc_init(enum IPCEndpointType type);
int ipc_recv(int s, struct IPCMessage *m);
int ipc_send(int s, const struct IPCMessage *msg);
int ipc_find(int s, const struct IPCFindRequest *req, struct IPCFindResponse *rsp);


#ifdef __cplusplus
}
#endif

#endif
