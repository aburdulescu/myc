## Myc

MycelNet(Mycelium Network) is a peer-to-peer secure network.

### Installation

#### Compile sources and install binaries

Dependencies:
- build-essential(gcc, make etc.)
- cmake
- OpenSSL
- LibEvent
- ncurses
- glog

Instructions:
- download the sources: `git clone https://gitlab.com/aburdulescu/lmesh.git`
- compile: `make`
- install: `sudo make install`

Usage:
- wip

#### Build docker image and run the container

Dependencies:
- build-essential(gcc, make etc.)
- docker

Instructions:
- build base image(which will contain openssl, libevent etc.): `cd docker_base; make`
- build image: `make docker`

Usage:
- start container: `docker run -it 'image_name'`

### Architecture details

#### Protocols used
- dht for peer discovery;
- t.b.d. for p2p message exchange.

#### Components
1. Daemon:
    - takes care of all routing(runs dht instance) and receives commands from the cli.

2. Cli:
    - Implements the following commands:
        - **init**: generate RSA keypair, generate nodeid, save configurations to disk, etc.
        - **info**: print information about the node
        - **send** *nodeId* *message*: send *message* to the node associated with *nodeId*
