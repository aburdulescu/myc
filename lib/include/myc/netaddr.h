#ifndef __MYC_DHT_NETADDR__
#define __MYC_DHT_NETADDR__

#include <netinet/in.h>


#ifdef __cplusplus
extern "C"{
#endif

#define NETADDR_LENGTH INET_ADDRSTRLEN

struct NetworkAddress
{
    char ip[NETADDR_LENGTH];
    int port;
};

int dht_netaddr_ntop(struct sockaddr_in *n_addr, struct NetworkAddress *p_addr);
int dht_netaddr_equals(const struct NetworkAddress *addr0, const struct NetworkAddress *addr1);

    
#ifdef __cplusplus
}
#endif

#endif
