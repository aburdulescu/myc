#ifndef __MYC_NODEID__
#define __MYC_NODEID__

#ifdef __cplusplus
extern "C"{
#endif

#define MYC_NODE_ID_LENGTH 20
#define MYC_HASH_ALGORITHM "sha1"
#define NODE_ID_PSTRING_LENGTH (2*MYC_NODE_ID_LENGTH+1)

typedef unsigned char NodeId[MYC_NODE_ID_LENGTH];

int  nodeid_new(NodeId nid, const unsigned char *data, const int data_sz);
void nodeid_btop(const NodeId nid, char out[2*MYC_NODE_ID_LENGTH]);
int  nodeid_ptob(const char *pnid, const int pnid_len, NodeId nid);
int  nodeid_prefixLength(const NodeId nid);
void nodeid_xor(const NodeId nid0, const NodeId nid1, NodeId res_nid);
int  nodeid_equals(const NodeId nid0, const NodeId nid1);
int  nodeid_compare(const NodeId nid0, const NodeId nid1);
void nodeid_copy(NodeId dest, const NodeId src);

#ifdef __cplusplus
}
#endif

#endif
