#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "myc/cmn.h"
#include "myc/config.h"
#include "myc/nodeid.h"
#include "myc/ipc.h"

// Type definitions
typedef int (*cmd_callback_t)(int argc, char *argv[]);

struct cmd
{
    char *name;
    int argc;
    cmd_callback_t cb;
    char *description;
};

// Prototype definitions
static int parse_args(int argc,
                      char *argv[],
                      cmd_callback_t *cb);

static int cmd_help(int argc, char *argv[]);
static int cmd_init(int argc, char *argv[]);
static int cmd_info(int argc, char *argv[]);

// Global variables
const struct cmd g_commands[] = 
{
    {"help", 0, cmd_help, "print this message"},
    {"init", 0, cmd_init, "initialize the node"},
    {"info", 0, cmd_info, "print information about the node"},
};

const int G_COMMANDS_SIZE = sizeof(g_commands)/sizeof(struct cmd);

struct config_variables cfg_vars;

char *PROG_NAME;

int main(int argc, char *argv[])
{
    PROG_NAME = argv[0];

    get_config_variables(&cfg_vars);

    cmd_callback_t cmd;
    if(parse_args(argc, argv, &cmd) < 0)
        return 1;

    cmd(argc-2, &argv[2]);

    return 0;
}

static int parse_args(int argc,
                      char* argv[],
                      cmd_callback_t *cb)
{
    if(argc == 1)
    {
        printf("No arguments given.\nEnter %s help for help.\n",
               PROG_NAME);
        return -1;
    }

    char *CMD = argv[1];

    // decrement used args(i.e. filename and command)
    argc -= 2;

    int i;
    for(i = 0;
        strcmp(g_commands[i].name, CMD) != 0 && i < G_COMMANDS_SIZE;
        ++i);

    if(i == G_COMMANDS_SIZE)
    {
        printf("Action %s is not supported.\nEnter %s help for help.\n",
               CMD, PROG_NAME);
        return -1;
    }

    if(g_commands[i].argc != argc)
    {
        printf("Command '%s' needs %d arguments\nEnter %s help for help.\n",
               CMD, g_commands[i].argc, PROG_NAME);
        return -1;
    }

    *cb = g_commands[i].cb;

    return 0;
}

static int cmd_help(int argc, char *argv[])
{
    printf("USAGE:\n");
    printf("\n");
    printf("myc command [ARG]\n");
    printf("\n");
    printf("Command:\n");
    for(int i = 0; i < G_COMMANDS_SIZE; ++i)
    {
        printf("\t%s: %s\n", g_commands[i].name, g_commands[i].description);
    }
    return 0;
}

static int is_dir(const char *path)
{
    struct stat s = {0};

    if (!stat(path, &s))
    {
        return S_ISDIR(s.st_mode);
    }
    else
    {
        return 0;
    }
}

static int crypto_generate_key(const char *private_key_filepath, const char *public_key_filepath)
{
    int     ret        = 0;
    BIGNUM *bne        = NULL;
    RSA    *r          = NULL;
    BIO    *bp_public  = NULL;
    BIO    *bp_private = NULL;

    bne = BN_new();
	ret = BN_set_word(bne, RSA_F4);
	if(ret != 1)
    {
        goto cleanup;
    }

    r = RSA_new();
    ret = RSA_generate_key_ex(r, MYC_KEY_SIZE, bne, NULL);
    if(ret != 1)
    {
        goto cleanup;
    }

    bp_public = BIO_new_file(public_key_filepath, "w+");
    ret = PEM_write_bio_RSAPublicKey(bp_public, r);
    if(ret != 1)
    {
        goto cleanup;
    }

    bp_private = BIO_new_file(private_key_filepath, "w+");
    ret = PEM_write_bio_RSAPrivateKey(bp_private, r, NULL, NULL, 0, NULL, NULL);

cleanup:
    BIO_free_all(bp_public);
    BIO_free_all(bp_private);
    RSA_free(r);
    BN_free(bne);

    return (ret == 1) ? 0 : -1;
}

static int crypto_read_key(const char *filepath, unsigned char **data, int *data_sz)
{
    int  rc  = 0;
    RSA *key = NULL;
    BIO *bp  = NULL;

    bp = BIO_new_file(filepath, "r+");
	key = PEM_read_bio_RSAPublicKey(bp, &key, NULL, NULL);
    if(key == NULL)
    {
        printf("error reading Public key\n");
        rc = -1;
        goto cleanup;
    }

    *data_sz = i2d_RSA_PUBKEY(key, data);
    if(*data_sz == -1
       || *data == NULL)
    {
        printf("error: %s: data == NULL\n", __FUNCTION__);
        rc = -1;
        goto cleanup;
    }

cleanup:
    RSA_free(key);
    BIO_free(bp);

    return rc;
}

// generate RSA key pair
// store key
// generate node_id
static int cmd_init(int argc, char *argv[])
{
    if(!is_dir(cfg_vars.config_dirpath))
    {
        printf("Initializing node in %s\n", cfg_vars.config_dirpath);
        int rc = mkdir(cfg_vars.config_dirpath, S_IRUSR|S_IWUSR|S_IXUSR);
        if(rc == 0)
        {
            printf("Generating RSA key pair ... ");
            if(crypto_generate_key(cfg_vars.private_key_filepath, cfg_vars.public_key_filepath) == -1)
                return -1;
            printf("done\n");

            unsigned char *key_data    = NULL;
            int            key_data_sz = 0;

            if(crypto_read_key(cfg_vars.public_key_filepath, &key_data, &key_data_sz) == -1)
            {
                printf("error: crypto_read_key()\n");
                return -1;
            }

            NodeId nid;
            if(nodeid_new(nid, key_data, key_data_sz) == -1)
                return -1;
            char nid_printable[2*MYC_NODE_ID_LENGTH];
            nodeid_btop(nid, nid_printable);
            printf("Node identity: %s\n", nid_printable);

            FILE *fp = fopen(cfg_vars.node_id_filepath, "wb");
            if(fp == NULL)
            {
                perror("fopen");
                return -1;
            }

            fwrite(nid, sizeof(unsigned char), MYC_NODE_ID_LENGTH, fp);
            fclose(fp);
        }
        else
        {
            printf("mkdir: %s\n", strerror(errno));
            return -1;
        }
    }
    else
    {
        printf("Config already done.\n");
    }
    return 0;
}

// print node info
static int cmd_info(int argc, char *argv[])
{
    if(is_dir(cfg_vars.config_dirpath))
    {
        FILE *fp = fopen(cfg_vars.node_id_filepath, "rb");
        if(fp == NULL)
        {
            printf("error: %s\n", strerror(errno));
        }

        NodeId nid;
        fread(nid, sizeof(unsigned char), MYC_NODE_ID_LENGTH, fp);

        char nid_printable[2*MYC_NODE_ID_LENGTH];
        nodeid_btop(nid, nid_printable);
        printf("Node identity: %s\n", nid_printable);

        fclose(fp);
    }
    else
    {
        printf("error: Node is not initialized.\n");
        return -1;
    }

    return 0;
}
