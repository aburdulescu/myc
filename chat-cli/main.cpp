#include <iostream>
#include <string.h>
#include <unistd.h>
#include <glog/logging.h>

#include "chatbe.h"
#include "myc/nodeid.h"
#include "myc/ipc.h"

#include "chatui.h"
#include "chatmq.h"

int main(int argc, char *argv[])
{
    google::InitGoogleLogging(argv[0]);

    ChatBackend *backend = new ChatBackend();
    ChatUI *ui = new ChatUI();

    backend->run(ui->getMQ());
    ui->run(backend->getMQ());

    delete backend;
    delete ui;

    return 0;
}
