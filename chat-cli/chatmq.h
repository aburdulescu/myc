#ifndef __MYC_CHATMQ__
#define __MYC_CHATMQ__

#include <string>
#include <queue>
#include <pthread.h>

enum ChatMQMessageId
{
    MESSAGEID_CONNECT_REQ,
    MESSAGEID_CONNECT_RSP,
    MESSAGEID_CONNECT_ERR,
    MESSAGEID_SEND_REQ,
    MESSAGEID_SEND_RSP,
    MESSAGEID_SEND_ERR,
    MESSAGEID_RECV_REQ,
    MESSAGEID_RECV_RSP,
    MESSAGEID_EXIT_REQ,
    MESSAGEID_EXIT_RSP,
};

struct ChatMQMessage
{
    ChatMQMessageId id;
    std::string data;
};

class ChatMQ
{
public:
    ChatMQ();
    ~ChatMQ();

    void push(const ChatMQMessage& d);
    bool empty();
    bool try_pop(ChatMQMessage& d);
    void wait_and_pop(ChatMQMessage& d);

private:
    std::queue<ChatMQMessage> q_;
    pthread_cond_t cv_;

    pthread_mutex_t q_mutex_;
    pthread_mutex_t cv_mutex_;

    pthread_mutexattr_t q_mutex_attr_;
    pthread_mutexattr_t cv_mutex_attr_;

};

#endif
