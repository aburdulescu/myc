#include "chatbe.h"

#include <sys/socket.h>
#include <netinet/ip.h>
#include <unistd.h>
#include <iostream>
#include <glog/logging.h>

#include "myc/cmn.h"
#include "myc/ipc.h"

ChatBackend::ChatBackend()
{
    pthread_attr_init(&thread_attr_);
    pthread_attr_setdetachstate(&thread_attr_, PTHREAD_CREATE_DETACHED);

    thread_ = -1;

    mq_ = new ChatMQ();
}

ChatBackend::~ChatBackend()
{
    close(socket_);
}

void ChatBackend::run(ChatMQ *ui_mq)
{
    StartArgs *rargs = new StartArgs();
    rargs->pthis = this;
    rargs->ui_mq = ui_mq;
    pthread_create(&thread_, &thread_attr_, entry, rargs);
}

ChatMQ* ChatBackend::getMQ()
{
    return mq_;
}

void* ChatBackend::entry(void *args)
{
    StartArgs *rargs = static_cast<StartArgs *>(args);
    rargs->pthis->start(rargs->ui_mq);
    delete rargs;
    return NULL;
}

void ChatBackend::start(ChatMQ *ui_mq)
{
    LOG(INFO) << "BE thread started";

    bool exitNeeded = false;
    while(!exitNeeded)
    {
        ChatMQMessage d;
        mq_->wait_and_pop(d);

        LOG(INFO) << "new message: id=" << d.id << " data=" << d.data;

        switch(d.id) {
        case MESSAGEID_EXIT_REQ:
        {
            d = {MESSAGEID_EXIT_RSP, ""};
            LOG(INFO) << "sending response: id=" << d.id << " data=" << d.data;
            ui_mq->push(d);
            exitNeeded = true;
            break;
        }
        case MESSAGEID_SEND_REQ:
        {
            d = {MESSAGEID_SEND_RSP, ""};
            LOG(INFO) << "sending response: id=" << d.id << " data=" << d.data;
            ui_mq->push(d);
            break;
        }
        case MESSAGEID_CONNECT_REQ:
        {
            const char *peer_id_printable = d.data.c_str();
            NodeId peer_id;

            if(nodeid_ptob(peer_id_printable, strlen(peer_id_printable), peer_id) == -1)
            {
                LOG(INFO) << "the provided peer id is not a valid peer id";
                d = {
                    MESSAGEID_CONNECT_ERR,
                    "requested id is not valid"
                };
                LOG(INFO) << "sending response: id=" << d.id << " data=" << d.data;
                ui_mq->push(d);

                break;
            }

            LOG(INFO) << "finding network address for "<< peer_id_printable << " ...";

            struct NetworkAddress peer_addr;
            peer_addr.port = ChatBackend::PORT;
            if(findPeer(peer_id, peer_addr) == -1)
            {
                LOG(INFO) << "no peer found with id: " << peer_id_printable;
                d = {
                    MESSAGEID_CONNECT_ERR,
                    "no peer found with requested id"
                };
                LOG(INFO) << "sending response: id=" << d.id << " data=" << d.data;
                ui_mq->push(d);

                break;
            }

            LOG(INFO) << "peer address is " << peer_addr.ip << ":" << peer_addr.port;

            d = {
                MESSAGEID_CONNECT_RSP,
                "connected"
            };
            LOG(INFO) << "sending response: id=" << d.id << " data=" << d.data;
            ui_mq->push(d);

            break;
        }
        default:
            break;
        }
    }

    LOG(INFO) << "exiting";
    pthread_exit(NULL);
}

int ChatBackend::findPeer(const NodeId id, struct NetworkAddress& addr)
{
    int s = ipc_init(IPC_ENDPOINT_TYPE_CLIENT);
    if(s == -1)
    {
        LOG(INFO) << "[.]--ipc_init failed\n";
        return -1;
    }
    struct IPCFindRequest req;
    nodeid_copy(req.id, id);

    struct IPCFindResponse rsp;
    if(ipc_find(s, &req, &rsp) == -1)
    {
        return -1;
    }

    if(strlen(rsp.addr.ip) == 0
       && rsp.addr.port == 0)
    {
        close(s);
        return -1;
    }
    else
    {
        memcpy(addr.ip, rsp.addr.ip, NETADDR_LENGTH);
        close(s);
        return 0;
    }
}
