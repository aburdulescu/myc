#include "chatmq.h"

#include <iostream>
#include <errno.h>
#include <string.h>
#include <glog/logging.h>


ChatMQ::ChatMQ()
{
    pthread_mutexattr_init(&q_mutex_attr_);
    pthread_mutexattr_settype(&q_mutex_attr_, PTHREAD_MUTEX_ERRORCHECK);
    pthread_mutex_init(&q_mutex_, &q_mutex_attr_);

    pthread_mutexattr_init(&cv_mutex_attr_);
    pthread_mutexattr_settype(&cv_mutex_attr_, PTHREAD_MUTEX_ERRORCHECK);
    pthread_mutex_init(&cv_mutex_, &cv_mutex_attr_);

    pthread_cond_init(&cv_, NULL);
}

ChatMQ::~ChatMQ()
{
    pthread_mutex_destroy(&q_mutex_);
    pthread_mutexattr_destroy(&q_mutex_attr_);

    pthread_mutex_destroy(&cv_mutex_);
    pthread_mutexattr_destroy(&cv_mutex_attr_);

    pthread_cond_destroy(&cv_);
}

void ChatMQ::push(const ChatMQMessage& d)
{
    pthread_mutex_lock(&q_mutex_);
    q_.push(d);
    pthread_mutex_unlock(&q_mutex_);
    pthread_cond_signal(&cv_);
}

bool ChatMQ::empty()
{
    int rc = pthread_mutex_lock(&q_mutex_);
    if(rc != 0)
        LOG(INFO) << "error: " << strerror(rc);
    bool isEmpty = q_.empty();
    pthread_mutex_unlock(&q_mutex_);
    return isEmpty;
}

bool ChatMQ::try_pop(ChatMQMessage& d)
{
    bool isEmpty = empty();
    if(!isEmpty)
    {
        pthread_mutex_lock(&q_mutex_);
        d = q_.front();
        q_.pop();
        pthread_mutex_unlock(&q_mutex_);
    }

    return isEmpty;
}

void ChatMQ::wait_and_pop(ChatMQMessage& d)
{
    while(empty())
    {
        pthread_mutex_lock(&cv_mutex_);
        LOG(INFO) << "waiting for message ...";
        pthread_cond_wait(&cv_, &cv_mutex_);
    }

    LOG(INFO) << "q is not empty => get message";

    pthread_mutex_lock(&q_mutex_);
    d = q_.front();
    q_.pop();
    pthread_mutex_unlock(&q_mutex_);
}
