FROM myc-base

ADD ./ /myc

RUN cd /myc && make && make install && rm -rf /myc
