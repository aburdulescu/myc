#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include "dht.h"
#include "myc/nodeid.h"


static int get_rand_string(char *output, int sz, unsigned int seed)
{
    if(output == NULL)
        return 0;

    srand(seed);
    
    int r0 = 1000000000 + (rand() % 9000000000);
    r0 *= (r0 < 0) ? -1 : 1;

    snprintf(output, 10, "%d", r0);

    output[sz] = '\0';
    return r0;
}

// TODO: check if the returned nodes are the correct ones
void test_dht_rt_update(struct RoutingTable *rt)
{
    int HOW_MANY_NODES = 40000;
    for(int i = 0; i < HOW_MANY_NODES; ++i)
    {
        char rand_str[11];
        memset(rand_str, 0, 11);
        get_rand_string(rand_str, 11, i);

        struct Contact c;
        if(nodeid_new(c.id, (unsigned char *)rand_str, strlen(rand_str)) == -1)
        {
            printf("nodeid_new returned -1\n");
            return;
        }
        dht_rt_update(rt, &c);
    }
}

void test_dht_rt_find(const struct RoutingTable *rt, const NodeId id, ContactList contacts)
{
    dht_rt_find(rt, id, contacts);

    char id_p[NODE_ID_PSTRING_LENGTH];
    nodeid_btop(id, id_p);

    printf("Target = %s\n", id_p);

    printf("Closest %d nodes:\n", DHT_K_VALUE);
    for(int i = 0; i < DHT_K_VALUE; ++i)
    {
        char out0[NODE_ID_PSTRING_LENGTH];
        nodeid_btop(contacts[i].contact.id, out0);

        char out1[NODE_ID_PSTRING_LENGTH];
        nodeid_btop(contacts[i].distance, out1);

        printf("cl[%d] = %s, distance(cl[%d],target) = %s\n", i, out0, i, out1);
    }
}

void read_nodeid(NodeId id)
{
    FILE *fp = fopen("/home/aburdulescu/.myc/node_id", "rb");
    if(fp == NULL)
    {
        printf("error: %s\n", strerror(errno));
    }

    fread(id, sizeof(unsigned char), MYC_NODE_ID_LENGTH, fp);

    fclose(fp);

}

int main(int argc, char *argv[argc])
{
    NodeId id;
    read_nodeid(id);

    struct DHT dht;
    dht_init(&dht, id);

    test_dht_rt_update(&dht.rt);

//    dht_rt_print(&rt);

    ContactList clist;
    test_dht_rt_find(&dht.rt, id, clist);

    printf("Node address: %s:%d\n", dht.addr.ip, dht.addr.port);

    while(1)
    {
        struct NetworkAddress addr;
        if(dht_rpc_recv(dht.rpc_handle, NULL, &addr) == -1)
            return 1;

        printf("[]<-[%s:%d]\n", addr.ip, addr.port);

        if(dht_rpc_send(dht.rpc_handle, NULL, &addr) == -1)
            return 1;

        printf("[]->[%s:%d]\n", addr.ip, addr.port);
    }

    return 0;
}
